<?php /* @var $this Controller */ ?>
<?php //$this->beginContent('//layouts/main'); ?>
<!DOCTYPE html>

<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Sitio institucional">
    <meta name="author" content="Tobias Mauricio">

    <title>CideSys</title>
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/favicon.ico">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/css/agency.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

<!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top" style="padding: 0px;">
                    <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Cidesys-Logo.jpg" style="width: 40%;">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right" style="margin-top: 14px; margin-right: 50px;">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" style="font-size: 18px;" href="<?php echo Yii::app()->baseUrl . "/site/home"; ?>">Home</a>
                    </li>
                    <li>
                        <a class="page-scroll" style="font-size: 18px;" href="<?php echo Yii::app()->baseUrl . "/site/nosotros"; ?>">Nosotros</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" style="font-size: 18px;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Servicios <span class="caret"></span></a>
                        <ul class="dropdown-menu" style="left: 0px; min-width: 175px;">
                            <li style="margin: 12px 0px;"><a href="#">Empresas</a></li>
                            <li style="margin: 12px 0px;"><a href="<?php echo Yii::app()->baseUrl . "/site/hardandsoft"; ?>">Hard&Soft</a></li>
                            <li style="margin: 12px 0px;"><a href="<?php echo Yii::app()->baseUrl . "/site/redes"; ?>">Redes</a></li>
                            <li style="margin: 12px 0px;"><a href="<?php echo Yii::app()->baseUrl . "/site/soporte_tecnico"; ?>">Soporte T&eacute;cnico</a></li>
                            <li style="margin: 12px 0px;"><a href="#">Sicob Online</a></li>
                        </ul>

                    </li>
                    <li>
                        <a class="page-scroll" style="font-size: 18px;" href="<?php echo Yii::app()->baseUrl . "/site/clientes"; ?>">Clientes</a>
                    </li>
                    <li>
                        <a class="page-scroll" style="font-size: 18px;" href="<?php echo Yii::app()->baseUrl . "/site/contacto"; ?>">Contacto</a>
                    </li>
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

	<?php
        /* ------------------------------- */
        /* ---------- CONTENIDO ---------- */
        echo $content;
        /* ------------------------------- */
        /* ------------------------------- */
    ?>


    <footer id="mifooter">
        <div class="container">
            <div class="row">
                <div class="col-md-9 text-left">
                    <span class="copyright">2015 Cidesys. All Rights Reserved</span>
                </div>
                <div class="col-md-2">
                    <ul class="list-inline social-buttons" style="margin-top: 5px;">
                        <li><a href="https://www.facebook.com/cidesys?fref=ts" target="_blank"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="mailto:info@cidesys.com.ar"><i class="glyphicon glyphicon-envelope"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-1 text-right">
                    <a href="#arriba" class="btn" style="background-color: #444; padding: 2px 8px; margin-top: 6px;"><span class="glyphicon glyphicon-chevron-up" style="color: #FFF;"></span></a>
                </div>
            </div>
        </div>
    </footer>
</body><!-- content -->
<?php //$this->endContent(); ?>


<!-- jQuery -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/js/classie.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/js/cbpAnimatedHeader.js"></script>

<!-- Contact Form JavaScript -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/js/jqBootstrapValidation.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/js/contact_me.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/js/agency.js"></script>

<script type="text/javascript">

    $(document).ready(function() {
        if ($("#mensaje-info").val() != undefined) {
            alert($("#mensaje-info").val());
            $("#mensaje-info").remove();
        }
    });

</script>

</html>