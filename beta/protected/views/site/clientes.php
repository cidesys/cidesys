<div id="arriba">&nbsp;</div>
<div class="container" style="width: 100%; margin-top: 160px;">
    <div class="row">
        <div class="col-md-5">
            <div class="text-center" style="background-color: #5191d0; padding: 12px 0px; margin-left: -70px;">
                <label style="color: #FFF; font-size: 28px;">Nuestros Clientes</label>
            </div>
        </div>
    </div>
    <br/><br/><br/>
    <div class="row text-center">
        <div class="col-md-4">
            <a href="http://www.soldouttravel.com" target="_blank">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Logos-Clientes/image001.gif" style="width: 40%; margin: 40px 0 0 0;">
            </a>
        </div>
        <div class="col-md-4">
            <a href="http://www.cubana.cu" target="_blank">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Logos-Clientes/Cubana sin fondo.png" style="width: 40%; margin: 44px 0 0 0;">
            </a>
        </div>
        <div class="col-md-4">
            <a href="http://www.tiempolibreviajes.com.ar" target="_blank">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Logos-Clientes/TIEMPO_LIBRE.jpg" style="width: 40%;">
            </a>
        </div>
    </div>
    <br/><br/><br/>
    <div class="row text-center">
        <div class="col-md-4">
            <a href="http://www.jetonline.com.ar" target="_blank">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Logos-Clientes/1107229_Ar_logo_Turismo-Jet-de-Antartica.jpg" style="width: 40%; margin: 40px 0 0 0;">
            </a>
        </div>
        <div class="col-md-4">
            <a href="http://www.grupo8.com.ar" target="_blank">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Logos-Clientes/grupo8.jpg" style="width: 40%;">
            </a>
        </div>
        <div class="col-md-4">
            <a href="http://www.solways.com.ar" target="_blank">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Logos-Clientes/logo-solways.jpg" style="width: 40%;">
            </a>
        </div>
    </div>
    <br/><br/><br/>
    <div class="row text-center">
        <div class="col-md-4">
            <a href="http://www.americanexecutive.com.ar" target="_blank">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Logos-Clientes/logoazul.jpg" style="width: 40%;">
            </a>
        </div>
        <div class="col-md-4">
            <a href="http://www.guantanameraviajes.com.ar" target="_blank">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Logos-Clientes/guantanamera.jpg" style="width: 40%;">
            </a>
        </div>
        <div class="col-md-4">
            <a href="http://www.flytap.com" target="_blank">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Logos-Clientes/Fly-Tap-Portugal-Logo-Vector.jpg" style="width: 40%;">
            </a>
        </div>
    </div>
    <br/><br/><br/>
    <div class="row text-center">
        <div class="col-md-4">
            <a href="http://www.itca.com.ar" target="_blank">www.itca.com.ar</a>
        </div>
        <div class="col-md-4">
            <a href="http://www.nauturismo.com.ar" target="_blank">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Logos-Clientes/descarga.jpg" style="width: 40%;">
            </a>
        </div>
        <div class="col-md-4">
            <br/><a href="http://www.ipsj.edu.ar" target="_blank">www.ipsj.edu.ar</a>
        </div>
        <div class="col-md-4">
            <br/><a href="http://www.algipel.com.ar" target="_blank"><img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Logos-Clientes/algipel.png" style="width: 40%;"></a>
        </div>
    </div>
    <br/><br/><br/>
    <div class="row text-center">
        <div class="col-md-4">
            <a href="http://www.stp.org.ar" target="_blank"><img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Logos-Clientes/sindicato.jpg" style="width: 40%;"></a>
        </div>
        <div class="col-md-4">
            <a href="http://www.destinet.com.ar" target="_blank"><img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Logos-Clientes/destinet_logo.png" style="width: 40%;"></a>
        </div>
        <div class="col-md-4">
            <a href="http://www.piamonteonline.com.ar" target="_blank"><img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Logos-Clientes/logo_piamo_home.jpg" style="width: 40%;"></a>
        </div>
    </div>
    <br/><br/><br/>
    <div class="row text-center">
        <div class="col-md-4">&nbsp;</div>
        <div class="col-md-4">
            <a href="#" target="_blank">4Continente</a>
        </div>
        <div class="col-md-4">

        </div>
    </div>

</div>
