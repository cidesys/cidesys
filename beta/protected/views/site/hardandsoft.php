<div id="arriba">&nbsp;</div>
<div class="container" style="width: 100%; margin-top: 160px;">
    <div class="row">
        <div class="col-md-5">
            <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Hard&Soft.jpg" style="width: 100%; margin-right: 20px;">

            <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Hard&soft-Icono.png" style="width: 50%; margin: 150px 0 0 25%;">

            <div class="text-center" style="background-color: #5191d0; padding: 12px 0px; margin-top: 150px; margin-left: -70px; font-size: 22px;">
                <label style="color: #FFF; margin-right: 70px;">&iquest;Necesitas Ayuda?</label>
            </div>
            <div class="text-center" style="background-color: #3071b8; width: 86%; padding: 12px 0px; margin-left: -70px; margin-bottom: 100px;">
                <a href="<?php echo Yii::app()->baseUrl . "/site/contacto"; ?>" class="btn btn-primary" >Contactanos</a>
            </div>
        </div>
        <div class="col-md-1">&nbsp;</div>
        <div class="col-md-5" style="margin-bottom: 100px;">
            <h3 style="color: #5191d0;">Hardware</h3>
            <p>
                - Armado, programado y dise&ntilde;o de equipos nuevos y usados.
                <br/><br/>
                - Verificaci&oacute;n de equipos existentes, prueba de funcionamiento de perif&eacute;ricos instalados al mismo.
                <br/><br/>
                - Funcionamiento de Impresoras general (Salvo que haya previamente contra-tado un servicio de mantenimiento con otra empresa. No incluye insumos)
                <br/><br/>
                - Verificaci&oacute;n conectividad en cables de alimentaci&oacute;n y/o conectores (Salvo que haya previamente contratado un servicio de mantenimiento con otra em-presa. No incluye insumos)
                <br/><br/>
                - Asesoramiento permanente sobre adquisici&oacute;n de tecnolog&iacute;as nuevas, equipos, impresoras, y dem&aacute;s equipos.
                <br/><br/>
                - En caso de falla de hardware las mismas se solucionan con partes originales nuevas y/o usadas (verificadas) con garant&iacute;a del proveedor en cuesti&oacute;n.
                <br/><br/>
                - Control y mantenimiento l&oacute;gico de la performance de los equipos de la empresa.
                Sugerencias sobre actualizaci&oacute;n de los equipos de la empresa.
            </p>

            <br/>

            <h3 style="color: #5191d0;">Software</h3>
            <p>
                - Instalaci&oacute;n del sistema operativo en los equipos de la empresa. (Valores de licencia y dem&aacute;s del mismo se expresan por separado y son con cargo).
                <br/><br/>
                - Mantenimiento de los software (de ahora en adelante ?softs?), actualizaci&oacute;n, configuraci&oacute;n, verificaci&oacute;n del rendimiento.
                <br/><br/>
                - En caso de falla se verifica el problema, se explica lo ocurrido al usuario y solucionara, ya sea reinstalando o haciendo las acciones que se requieran.
                <br/><br/>
                - Instalaci&oacute;n de paquetes de Ofim&aacute;tica completos ya sea con licencia gratuita y/o paga con cargo. Adem&aacute;s se instalan los programas necesarios para que funcionen o vinculen con otros, llamados "plug-in".
                <br/><br/>
                - Asesoramiento permanente sobre la adquisici&oacute;n de nuevos programas y/o actualizaci&oacute;n de los existentes para resolver alg&uacute;n problema puntual. Prueba en nuestro laboratorio del software para homologarlo (seg&uacute;n est&aacute;ndares pre-definidos) aprobando la instalaci&oacute;n del mismo en los equipos.
                <br/><br/>
                - Reinstalaciones sin costo agregado para los equipos de la empresa que est&eacute;n incluidos en el abono exceptuando los servidores.
                <br/><br/>
                - Instalaci&oacute;n de Antiv&iacute;rus y Antispyware gratuito (Salvo que l&aacute; empresa tenga um contrato com uma empresa provedora de Software de protecci&oacute;n).
            </p>
        </div>

    </div>
</div>
