<?php if (isset($mensaje)) { ?>
    <input type="hidden" id="mensaje-info" value="<?php echo $mensaje; ?>" />
<?php } ?>

<!-- Header -->
<header id="arriba" style="">
    <div class="container" style="width: 100%; padding: 0px;">
        <div class="intro-text">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Motherboard-banner-web.jpg" style="width: 100%;">
                        <div class="carousel-caption" style="padding-bottom:6%;">
                            <p style="font-size: 40px;">Consultora Integral de<br/>Sistemas y Servicios</p>
                            <br/>
                            <a href="<?php echo Yii::app()->baseUrl . "/site/nosotros"; ?>" class="btn btn-primary" style="width: 150px;">Nosotros</a>
                        </div>
                    </div>
                    <div class="item">
                        <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Motherboard-banner-web.jpg" style="width: 100%;">
                        <div class="carousel-caption" style="padding-bottom:6%;">
                            <p style="font-size: 40px;">Consultora Integral de<br/>Sistemas y Servicios</p>
                            <br/>
                            <a href="<?php echo Yii::app()->baseUrl . "/site/nosotros"; ?>" class="btn btn-primary" style="width: 150px;">Nosotros</a>
                        </div>

                    </div>
                    <div class="item">
                        <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Motherboard-banner-web.jpg" style="width: 100%;">
                        <div class="carousel-caption" style="padding-bottom:6%;">
                            <p style="font-size: 40px;">Consultora Integral de<br/>Sistemas y Servicios</p>
                            <br/>
                            <a href="<?php echo Yii::app()->baseUrl . "/site/nosotros"; ?>" class="btn btn-primary" style="width: 150px;">Nosotros</a>
                        </div>

                    </div>
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</header>

<!-- Services Section -->
<section id="services" style="margin-bottom: 140px; margin-top: 140px">
    <div class="container">
        <!--div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Services</h2>
                <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
            </div>
        </div-->
        <div class="row text-left">
            <div class="col-md-2">&nbsp;</div>
            <div class="col-md-4">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Hard&soft-Icono.png" style="float: left; margin-bottom: 80px; margin-right: 20px;">
                <h4 class="service-heading">Hardware & Software</h4>
                <p class="text-muted">Productos, Insumos y programas de primer nivel. Asesoramiento, control y puesta en funcionamiento. Trabajamos con las mejores marcas del mercado.</p>
            </div>
            <div class="col-md-4">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Redes-Icono.png" style="float: left; margin-bottom: 80px; margin-right: 20px;">
                <h4 class="service-heading">Redes</h4>
                <p class="text-muted">Compartir los recursos y la informaci&oacute;n en la distancia, asegurar la confiabilidad y la disponibilidad de la informaci&oacute;n, aumentar la velocidad de transmisi&oacute;n de los datos.</p>
            </div>
        </div>
        <div class="row text-left" style="margin-top: 50px;">
            <div class="col-md-4">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Empresas-Icono.png" style="float: left; margin-bottom: 80px; margin-right: 20px;">
                <h4 class="service-heading">Empresas</h4>
                <p class="text-muted">Atencion especializada y soporte integral para su empresa. Teniendo como objetivo un mejor funcionamiento y expansi&oacute;n.</p>
            </div>
            <div class="col-md-4">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Soporte-Tecnico-Icono.png" style="float: left; margin-bottom: 80px; margin-right: 20px;">
                <h4 class="service-heading">Soporte T&eacute;cnico</h4>
                <p class="text-muted">Resoluci&oacute;n inmediata y exacta de problemas que puedan presertarse al usuario mientras hacen uso de servicios, programas y dispositivos.</p>
            </div>
            <div class="col-md-4">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/sicob-Icono.png" style="float: left; margin-bottom: 80px; margin-right: 20px;">
                <h4 class="service-heading">Sicob Online</h4>
                <p class="text-muted">Sistema de Gestion integral para obras soaciales y asociaciones mutuales. Una solucion tecnologica al alcance de un click.</p>
            </div>
        </div>
    </div>
</section>

<!-- Portfolio Grid Section -->
<section id="portfolio" class="bg-light-gray" >
    <div class="container" style="width: 100%;">
        <div class="row" style="margin-top: 10px;">
            <div class="col-lg-7 text-right">
                <h3 class="section-heading" style="color: #FFF; margin-top: 10px;">&iquest;Necesitas Asesoramiento?</h3>
            </div>
            <div class="col-lg-5 text-left" style="margin-top: 8px;">
                <a href="<?php echo Yii::app()->baseUrl . "/site/contacto"; ?>" class="btn btn-primary" >Contactanos</a>
            </div>
        </div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-4" style="padding: 0px;">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/software-standards-development-web.jpg" style="width: 100%; height: 240px;">
            </div>
            <div class="col-md-4" style="padding: 0px;">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Redes-3-web.jpg" style="width: 100%; height: 240px;">
            </div>
            <div class="col-md-4" style="padding: 0px;">
                <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/soporte1-web.jpg" style="width: 100%; height: 240px;">
            </div>
        </div>
    </div>
</section>
