<div id="arriba">&nbsp;</div>
<div class="container" style="width: 100%; margin-top: 160px;">
    <div class="row">
        <div class="col-md-5">
            <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/nosotros.jpg" style="width: 100%; margin-right: 20px;">
            <div class="text-center" style="background-color: #5191d0; padding: 12px 0px; margin-top: 200px; margin-left: -70px; font-size: 22px;">
                <label style="color: #FFF; margin-right: 70px;">&iquest;Necesitas Ayuda?</label>
            </div>
            <div class="text-center" style="background-color: #3071b8; width: 86%; padding: 12px 0px; margin-left: -70px; margin-bottom: 100px;">
                <a href="<?php echo Yii::app()->baseUrl . "/site/contacto"; ?>" class="btn btn-primary" >Contactanos</a>
            </div>
        </div>
        <div class="col-md-1">&nbsp;</div>
        <div class="col-md-5" style="margin-bottom: 100px;">
            <h3 style="color: #5191d0;">Sobre Nosotros</h3>
            <p>Somos grupo interdisciplinario operativo, de asesoramiento y servicio. Nuestro objetivo fundamental es asistir fundamentalmente a PYMES, Obras Sociales, Asociaciones Mutuales, etc. en &aacute;reas y actividades clave para su funcionamiento y posible expansi&oacute;n, proponiendo formas eficientes de auditoria, optimizaci&oacute;n y establecimiento de normas administrativas y operacionales, donde la Asociaci&oacute;n o Empresa lo requieran, sea en el &aacute;mbito de la Capital Federal, Gran Buenos Aires o en el interior del pa&iacute;s.
                <br/><br/>
                Nuestra estrategia principal es lograr que nuestros clientes sean m&aacute;s competitivos y exitosos. Para esto, nos unimos a ellos y planeamos a corto, mediano y largo plazo el desarrollo y la optimizaci&oacute;n de su plataforma inform&aacute;tica.
            </p>

            <br/>

            <h3 style="color: #5191d0;">&iquest;Por qu&eacute; CideSys?</h3>
            <p>
                Porque tendr&aacute; un socio tecnol&oacute;gico estrat&eacute;gico, quien lo ayudar&aacute; a mantener preventivamente su plataforma inform&aacute;tica, servidores.
                <br/><br/>
                Porque cuenta con infraestructura, procedimientos, recursos y personal capacitado en servicios de atenci&oacute;n al cliente para lograr la satisfacci&oacute;n total de los mismos.
                <br/><br/>
                Realizamos constantes revisiones para tener el detalle de los componentes de su plataforma inform&aacute;tica y poder optimizar la utilizaci&oacute;n de la misma.
                <br/><br/>
                La evoluci&oacute;n de nuestra empresa se ha basado en el establecimiento de relaciones de largo plazo con nuestros clientes, con el fin de proporcionarles una plataforma inform&aacute;tica estable y s&oacute;lida que brinde confianza, seguridad y tranquilidad a sus usuarios, esforz&aacute;ndonos en convertir los problemas que normalmente surgen en el mantenimiento de una plataforma.
            </p>

            <br/><br/>

            <h3 class="text-center" style="color: #5191d0;">Una nueva y eficiente propuesta IT para el desarrollo y optimizaci&oacute;n de su empresa.</h3>
        </div>

    </div>
</div>
