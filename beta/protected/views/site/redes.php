<div id="arriba">&nbsp;</div>
<div class="container" style="width: 100%; margin-top: 160px;">
    <div class="row">
        <div class="col-md-5">
            <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/net_working-web.jpg" style="width: 100%; margin-right: 20px;">

            <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Redes-Icono.png" style="width: 50%; margin: 150px 0 0 25%;">

            <div class="text-center" style="background-color: #5191d0; padding: 12px 0px; margin-top: 150px; margin-left: -70px; font-size: 22px;">
                <label style="color: #FFF; margin-right: 70px;">&iquest;Necesitas Ayuda?</label>
            </div>
            <div class="text-center" style="background-color: #3071b8; width: 86%; padding: 12px 0px; margin-left: -70px; margin-bottom: 100px;">
                <a href="<?php echo Yii::app()->baseUrl . "/site/contacto"; ?>" class="btn btn-primary" >Contactanos</a>
            </div>
        </div>
        <div class="col-md-1">&nbsp;</div>
        <div class="col-md-5" style="margin-bottom: 100px;">
            <h3 style="color: #5191d0;">Redes y Conectividad</h3>
            <p>
                Diagramado de red, topolog&iacute;as recomendadas y asistencia en la misma.
                <br/><br/>
                - Verificado de conectividad integral y parcial puesto por puesto.
                <br/><br/>
                - Sugerencias tanto en equipos de networking y/o ruteo de paquetes como en el armado de racks y/o conectividad de los mismos hacia el Switch de red.
                <br/><br/>
                - Verificado de velocidad integral de la red, conjunto servidor-networking. La misma se verifica en conexiones hacia Internet como en la red interna local de la empresa.
                <br/><br/>
                - Monitoreo constante de la red con distintas herramientas de software para verificaci&oacute;n de la performance.
            </p>

            <br/>

            <h3 style="color: #5191d0;">Energ&iacute;a (Resguardo)</h3>
            <p>
                - Asesoramiento en la adquisici&oacute;n de equipos de energ&iacute;a regulada).
                <br/><br/>
                - Asesoramiento en aspectos b&aacute;sicos de Electricidad y aplicaci&oacute;n de los mismos.
                <br/><br/>
                - Asesoramiento y elecci&oacute;n de equipos de electricidad no interrumpida (UPS).
            </p>

            <br/>

            <h3 style="color: #5191d0;">Servidores</h3>
            <p>
                - Armado Integro de Servidores de Producci&oacute;n, comunicaciones y networking.
                <br/><br/>
                - Accesos remotos al servidor y soluci&oacute;n de problemas a distancia.
                <br/><br/>
                - Configuraci&oacute;n de: sistema autom&aacute;tico de correos, Apagado o tareas programadas, Servidor de archivos, configuraci&oacute;n de red desde el servidor, sistema de backup, Ficheros compartidos, etc.
                <br/><br/>
                - Sistema de protecci&oacute;n por usuarios con permisos limitados.
                <br/><br/>
                - Sistema de seguridad provisto por firewall de 128 bits.
                <br/><br/>
                - Antivirus y Antispam propios del servidor.
            </p>

            <br/>

            <h3 style="color: #5191d0;">Backups</h3>
            <p>
                - Sistema autom&aacute;tico de Backup propiedad de CideSys.
                <br/><br/>
                - Sistema autom&aacute;tico de copia de correos propiedad de CideSys.
                <br/><br/>
                - Configuraci&oacute;n y monitoreo de los backups en los servidores de la empresa.
                <br/><br/>
                - Backup de 5 d&iacute;as h&aacute;biles acumulativo en ?frio? (horarios nocturnos).
            </p>

            <p style="color: #5191d0;">AMP | Furukawa | Hp |Cisco | Panduit | IBM</p>
        </div>

    </div>
</div>
