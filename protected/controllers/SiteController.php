<?php

class SiteController extends Controller {

    public $layout='//layouts/column1';


    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('home', 'nosotros', 'servicios', 'clientes', 'contacto', 'hardandsoft', 'redes', 'soporte_tecnico', "enviarMensaje"),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array(),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array(),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }



	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('home',array());

    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}


    public function actionHome(){
        $this->render('home',array(
            "aaa" => "aa"
        ));
        //echo CJavaScript::jsonEncode($this->layout);
        //Yii::app()->end();
    }

    public function actionNosotros() {

        $this->render('nosotros',array(
            "aaa" => "aa"
        ));
    }

    public function actionHardandsoft() {

        $this->render('hardandsoft',array(
            "aaa" => "aa"
        ));
    }

    public function actionRedes() {

        $this->render('redes',array(
            "aaa" => "aa"
        ));
    }

    public function actionSoporte_tecnico() {

        $this->render('soporte_tecnico',array(
            "aaa" => "aa"
        ));
    }

    public function actionClientes() {

        $this->render('clientes',array(
            "aaa" => "aa"
        ));
    }

    public function actionContacto() {
        $this->render('contacto',array(
            "aaa" => "aa"
        ));
    }


    public function actionEnviarMensaje() {
        // Check for empty fields
        if(empty($_POST['nombre']) ||
            empty($_POST['apellido']) ||
            empty($_POST['email']) ||
            empty($_POST['empresa']) ||
            empty($_POST['telefono']) ||
            empty($_POST['mensaje']))
        {
            echo "Faltan Campos Obligatorios!";
            return false;
        }

        $return = "La consulta ha sido enviada, te responderemos a la brevedad.";

        $name = $_POST['nombre'] . " " . $_POST['apellido'];
        $empresa = $_POST['empresa'];
        $phone = $_POST['telefono'];
        $email_address = $_POST['email'];

        $message = "Has recibido una nueva consulta de:<br/>".
        $name."<br/>".
        $empresa."<br/>".
        $phone."<br/>".
        $email_address."<br/><br/>".
        $_POST['mensaje'];

        // Create the email and send the message
        //$to = 'info@cidesys.com.ar';
        $to = Yii::app()->params["adminEmail"];
        $email_subject = "Consulta de: $name";
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: ".$_POST['email']."\n";
        mail($to,$email_subject,$message,$headers);


        $this->render('home',array(
            "mensaje" => $return,
        ));
    }

}