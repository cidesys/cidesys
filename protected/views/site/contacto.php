<div id="arriba">&nbsp;</div>
<div class="container" style="margin-top: 160px;">
    <div class="row">
        <div class="col-md-5">
            <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/information_technology-web.jpg" style="width: 100%; margin-right: 20px;">
            <div class="text-center" style="background-color: #5191d0; padding: 16px 0px; margin-top: 150px;">
                <div class="icono-redondo-blando" style="margin-left: 20px; float: left;"><i class="glyphicon glyphicon-envelope" style="color: #5191d0;"></i></div>
                <label style="color: #FFF; font-size: 23px; margin-right: 28px;">info@cidesys.com.ar</label>
            </div>
            <div class="text-center" style="background-color: #3071b8; width: 86%; padding: 16px 0px;">
                <div class="icono-redondo-blando" style="margin-left: 20px; float: left;"><i class="glyphicon glyphicon-earphone" style="color: #5191d0;"></i></div>
                <label style="color: #FFF; font-size: 23px;">(+5411) 4611 4576</label>
            </div>
        </div>
        <div class="col-md-7">
            <form id="frmConsulta" action="<?php echo Yii::app()->baseUrl; ?>/site/enviarMensaje" method="post" onsubmit="return $_validateDatosForm()">
                <form name="sentMessage" id="contactForm" novalidate>
                    <div class="row">
                        <div class="col-md-offset-1 col-md-10">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Nombre" name="nombre" required data-validation-required-message="Ingrese su nombre." style="border-color: #eee; background-color: #eee;">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 16px;">
                        <div class="col-md-offset-1 col-md-10">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Apellido" name="apellido" required data-validation-required-message="Ingrese su apellido." style="border-color: #eee; background-color: #eee;">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 16px;">
                        <div class="col-md-offset-1 col-md-10">
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email" name="email" required data-validation-required-message="Ingrese su email." style="border-color: #eee; background-color: #eee;">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 16px;">
                        <div class="col-md-offset-1 col-md-10">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Empresa" name="empresa" required data-validation-required-message="Ingrese empresa." style="border-color: #eee; background-color: #eee;">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 16px;">
                        <div class="col-md-offset-1 col-md-10">
                            <div class="form-group">
                                <input type="tel" class="form-control" placeholder="Telefono" name="telefono" required data-validation-required-message="Ingrese tel&eacute;fono." style="border-color: #eee; background-color: #eee;">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 16px;">
                        <div class="col-md-offset-1 col-md-10">
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Mensaje" name="mensaje" required data-validation-required-message="Ingrese un mensaje." rows="10" style="border-color: #eee; background-color: #eee;"></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-lg-12 text-center" style="margin-top: 50px; margin-bottom: 80px;">
                        <div id="success"></div>
                        <button type="submit" class="btn btn-primary" style="padding: 20px 30px;">Enviar Mensaje</button>
                    </div>

                </form>
            </form>
        </div>

    </div>
</div>

<script type="text/javascript">

    function $_validateDatosForm() {
        //Por ahora no pongo nada, veremos si despues falte alguna validacion.
        return true;
    }

</script>

