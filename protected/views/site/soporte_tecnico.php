<div id="arriba">&nbsp;</div>
<div class="container" style="width: 100%; margin-top: 160px;">
    <div class="row">
        <div class="col-md-5">
            <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/soporte1-web.jpg" style="width: 100%; margin-right: 20px;">

            <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/img/Soporte-Tecnico-Icono.png" style="width: 50%; margin: 150px 0 0 25%;">

            <div class="text-center" style="background-color: #5191d0; padding: 12px 0px; margin-top: 150px; margin-left: -70px; font-size: 22px;">
                <label style="color: #FFF; margin-right: 70px;">&iquest;Necesitas Ayuda?</label>
            </div>
            <div class="text-center" style="background-color: #3071b8; width: 86%; padding: 12px 0px; margin-left: -70px; margin-bottom: 100px;">
                <a href="<?php echo Yii::app()->baseUrl . "/site/contacto"; ?>" class="btn btn-primary" >Contactanos</a>
            </div>
        </div>
        <div class="col-md-1">&nbsp;</div>
        <div class="col-md-5" style="margin-bottom: 100px;">
            <h3 style="color: #5191d0;">Mantenimiento de Pc - Soporte Tecnico</h3>
            <p>
                En CideSys le brindamos un soporte t&eacute;cnico integral para su empresa.
                <br/><br/>
                <label style="color: #5191d0;">Nuestros abonos le permitir&aacute;n obtener:</label>
                <br/><br/>
                -  Visitas contempladas ilimitadas a su empresa durante el mes por cualquier tipo de evento y sin l&iacute;mite de horas.
                <br/><br/>
                - Mantenimiento preventivo en su empresa cada 15 d&iacute;as.
                <br/><br/>
                - Prioridad total en la atenci&oacute;n.
                <br/><br/>
                - Soporte de red.
                <br/><br/>
                - Actualizaci&oacute;n peri&oacute;dica de los antivirus en sus computadoras.
                <br/><br/>
                - Revisi&oacute;n de los backups en cada visita. Simulacros de recupero de informaci&oacute;n.
                <br/><br/>
                - Reportes escritos mensuales con el estado del sistema en cada visita del t&eacute;cnico.
                <br/><br/>
                - Censos de red y computadoras cada 6 meses con evaluaciones de proyectos y nuevas tecnolog&iacute;as disponibles.
                <br/><br/>
                - Asesor&iacute;a permanente en la tecnolog&iacute;a de la empresa.
            </p>

            <br/>

            <h3 style="color: #5191d0;">QUE GANAR&Aacute; Y EVITAR&Aacute; CON NUESTRO SERVICIO?</h3>
            <p>
                <label style="color: #5191d0;">Evitar&aacute;</label>
                <br/><br/>
                - Fallas intermitentes en los programas
                <br/><br/>
                - Molestias y riesgos por requerir llamar a t&eacute;cnicos freelance
                <br/><br/>
                - Costos ocultos en el presupuesto de tecnolog&iacute;a
                <br/><br/>
                - Perdidas de informaci&oacute;n por virus y otras amenazas
                <br/><br/>
                <label style="color: #5191d0;">Ganar&aacute;</label>
                <br/><br/>
                - Visitas ilimitadas a su empresa por cualquier evento.
                <br/><br/>
                - Soluci&oacute;n de los problemas de sus computadoras en 4 hs desde su llamada.
                <br/><br/>
                - Reducci&oacute;n de los tiempos muertos al m&iacute;nimo.
                <br/><br/>
                - Soporte remoto.
            </p>
        </div>

    </div>
</div>
